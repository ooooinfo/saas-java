package com.yhy.common.dao;

import com.yhy.common.vo.BusExtendVO;

import com.yhy.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-22 上午10:55 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-17 下午3:20 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Mapper
@Component(value = "busExtendDao")
public interface BusExtendDao  extends BaseDao<BusExtendVO> {

    BusExtendVO findByBusModuleAndId(@Param("busModule") String busModule, @Param("id") String id);

    BusExtendVO findByBusModuleAndMainId(@Param("busModule") String busModule,@Param("mainId") String mainId);

}
