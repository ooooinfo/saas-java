package com.yhy.common.dao;

import com.yhy.common.dto.BaseDTO;
import com.yhy.common.dto.BaseMngVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-12-12 下午6:18 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

public interface BaseComplexMngDao<T extends BaseDTO,E extends BaseMngVO> extends BaseMngDao<T, E> {

   List<E> findHistoryDataBySysCode(@Param("sysOwnerCpy") String sysOwnerCpy, @Param("sysGenCode")String sysGenCode, @Param("busModule")String busModule);

}
