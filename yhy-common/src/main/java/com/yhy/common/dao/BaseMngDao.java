package com.yhy.common.dao;

import com.yhy.common.dto.BaseDTO;
import com.yhy.common.dto.BaseMngVO;

import java.util.List;

/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */

public interface BaseMngDao<T extends BaseDTO,E extends BaseMngVO> {

   Long queryPageCount(T baseDTO);

   List<E> queryPage(T baseDTO);

}
