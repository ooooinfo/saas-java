package com.yhy.common.jxls;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.yhy.common.dto.BaseMngDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-23 下午8:27 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public abstract class MyJxlsImportSupport<T extends BaseMngDTO> {
	public final static String IMPORT_DATA_KEY = "data";
	protected Logger LOGGER = LoggerFactory.getLogger(getClass());

	private MyJxlsImportConfig jxlsImportConfig;

	private T baseDTO;

	public MyJxlsImportSupport() {
	}

	public T getBaseDTO() {
		return baseDTO;
	}

	public void setBaseDTO(T baseDTO) {
		this.baseDTO = baseDTO;
	}

	public MyJxlsImportSupport(MyJxlsImportConfig jxlsImportConfig) {
		this.jxlsImportConfig = jxlsImportConfig;
	}

	//自定义读取解析
	public boolean isCustomReader() {
		return false;
	}

	public Map<String, Object> initDatas(InputStream xlsDataStream) {
		LOGGER.info("模版数据解析开始");
		Map<String, Object> datas = Maps.newHashMap();
		datas.put(IMPORT_DATA_KEY, Lists.newArrayList());
		return datas;
	}

	public void handleData(Map<String, Object> datas) {
		LOGGER.info("模版数据解析完成，开始组装导入数据");
	}

	public MyJxlsImportConfig getJxlsImportConfig() {
		return jxlsImportConfig;
	}

	public void setJxlsImportConfig(MyJxlsImportConfig jxlsImportConfig) {
		this.jxlsImportConfig = jxlsImportConfig;
	}

}