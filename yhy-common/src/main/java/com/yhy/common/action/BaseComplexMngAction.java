package com.yhy.common.action;

import com.yhy.common.dto.AppReturnMsg;
import com.yhy.common.dto.BaseMngDTO;
import com.yhy.common.dto.BaseMngVO;
import com.yhy.common.dto.ReturnCode;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.service.BaseComplexMngService;
import com.yhy.common.service.BaseMngService;
import com.yhy.common.utils.BusCenterHelper;
import com.yhy.common.utils.ITransationExecutor;
import com.yhy.common.utils.YhyUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */

public abstract class BaseComplexMngAction<T extends BaseMngDTO> extends BaseMngAction<T> {

	protected abstract BaseComplexMngService getBaseService();

	@RequestMapping(value="/loadHistoryData",method = {RequestMethod.POST})
	@ResponseBody
	@ApiOperation(value="根据公司及系统编号获取历史版本数据", notes="根据公司及系统编号获取历史版本数据")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "paramBean.sysGenCode", value = "系统编号", required = true, dataTypeClass = BaseMngDTO.class),

	})
	public AppReturnMsg loadHistoryData(HttpServletRequest request, HttpServletResponse response,
										  @RequestBody T baseDTO) {
		BaseMngVO paramBean = (BaseMngVO) baseDTO.getParamBean();
		List<BaseMngVO> rtnData = getBaseService().findHistoryDataBySysCode(YhyUtils.getSysUser().getCpyCode(), paramBean.getSysGenCode());
		baseDTO.setRtnList(rtnData);
		AppReturnMsg returnMsg = new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"", baseDTO);
		return returnMsg;
	}

	protected AppReturnMsg toChangeCommon(HttpServletRequest request, HttpServletResponse response, T baseDTO) {
		getBaseService().toChangeData(baseDTO);
		AppReturnMsg returnMsg = new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"",baseDTO,null);
		afterPublicViewInfo(baseDTO,returnMsg);
		return returnMsg;
	}

	protected AppReturnMsg doDestoryCommon(HttpServletRequest request, HttpServletResponse response, T baseDTO) {
		if (!BusCenterHelper.isComplexBus(getBaseService().getBusModule())) {
			throw new BusinessException("此类业务不支持注销操作.");
		}
		if (CollectionUtils.isEmpty(baseDTO.getIdList())) {
			throw new BusinessException("业务ID不能为空.");
		}

		AppReturnMsg appReturnMsg = multTransactionExecute(request,response, baseDTO, new ITransationExecutor<Boolean,String>() {
			@Override
			public Boolean execute(String busId) {
				BaseMngVO busMainData = getBaseService().findByBusId(busId);
				getBaseService().doSingleDestory(baseDTO, busMainData);
				return true;
			}
		});
		return appReturnMsg;
	}

}
