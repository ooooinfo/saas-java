package com.yhy.common.service;

import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.SysUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yhy.common.dao.BusExtendDao;
import com.yhy.common.vo.BusExtendVO;
import com.yhy.common.service.BaseService;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-17 下午3:36 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-22 上午10:53 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class BusExtendService extends BaseService<BusExtendVO> {

	@Autowired
	private  BusExtendDao busExtendDao;

	@Override
	protected BusExtendDao getDao() {
		return busExtendDao;
	}


	@Override
	protected void preInsert(BusExtendVO entity) {
		if(StringUtils.isBlank(entity.getId())) {
			entity.setId(YhyUtils.genId(entity.getBusModule()));
		}
		super.preInsert(entity);
		if(entity.getVersion() == null) {
			entity.setVersion(1);
		}
		if (StringUtils.isBlank(entity.getEnableFlag())) {
			entity.setEnableFlag("Y");
		}
	}

	public Integer deleteBusExtendById(String id, String busModule) {
		BusExtendVO busExtendVO = new BusExtendVO();
		busExtendVO.setId(id);
		busExtendVO.setBusModule(busModule);
		return getDao().deleteById(busExtendVO);
	}

	public BusExtendVO findByBusModuleAndMainId(String busModule, String mainId) {
		return getDao().findByBusModuleAndMainId(busModule, mainId);
	}

	public BusExtendVO findByBusModuleAndId(String busModule, String id) {
		return getDao().findByBusModuleAndId(busModule, id);
	}
}
