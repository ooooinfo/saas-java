package com.yhy.common.vo;

import com.google.common.collect.Maps;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.utils.ToStringBean;

import java.io.Serializable;
import java.util.Map;

/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */

public class BusModuleSetting  extends ToStringBean implements Serializable  {

	private Map<String, BusModuleSettingItem> settingItems = Maps.newHashMap();

	public Map<String, BusModuleSettingItem> getSettingItems() {
		return settingItems;
	}

	public void setSettingItems(Map<String, BusModuleSettingItem> settingItems) {
		this.settingItems = settingItems;
	}

	public BusModuleSettingItem getSettingItemBySourceCode(String busModule) {
		if (!settingItems.containsKey(busModule)) {
			throw new BusinessException("busModule " + busModule + " is not invalid");
		}
		return settingItems.get(busModule);
	}

}
