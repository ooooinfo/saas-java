package com.yhy.common.utils;

import com.alibaba.fastjson.JSON;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.vo.BusModuleSetting;
import com.yhy.common.vo.BusModuleSettingItem;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */
public class BusCenterHelper {

	private final static String BUS_CENTER_SESSING_PATH = "busModuleSetting.json";

	/**
	 * 业务类型配置
	 */
	private static BusModuleSetting busModuleSetting;

	public static Boolean isComplexBus(String busModule) {
		return getBusSourceInfoByBusModuleCode(busModule).isComplexBus();
	}
	public static Boolean isNeedBarcode(String busModule) {
		return getBusSourceInfoByBusModuleCode(busModule).isNeedBarcode();
	}
	public static Boolean isSimpleHistoryMng(String busModule) {
		return getBusSourceInfoByBusModuleCode(busModule).isSimpleHistoryMng();
	}
	public static Boolean isAttachmentMng(String busModule) {
		return getBusSourceInfoByBusModuleCode(busModule).isAttachmentMng();
	}

	public static Boolean isOrgsetMng(String busModule) {
		return getBusSourceInfoByBusModuleCode(busModule).isOrgsetMng();
	}

	public static Boolean isNeedFeedback(String busModule) {
		return getBusSourceInfoByBusModuleCode(busModule).isNeedFeedback();
	}

	public static String mainTable(String busModule) {
		return getBusSourceInfoByBusModuleCode(busModule).getMainTable();
	}

	public static String sysAttachmentTable(String busModule) {
		return getBusSourceInfoByBusModuleCode(busModule).getSysAttachmentTable();
	}

	public static String busExtendTable(String busModule) {
		return getBusSourceInfoByBusModuleCode(busModule).getBusExtendTable();
	}

	public static String busCommonStateTable(String busModule) {
		return getBusSourceInfoByBusModuleCode(busModule).getBusCommonStateTable();
	}

	public static String busCommonStateHistoryTable(String busModule) {
		return getBusSourceInfoByBusModuleCode(busModule).getBusCommonStateHistoryTable();
	}

	public static String getBusCodePrefixByBusModule(String busModule) {
		return getBusSourceInfoByBusModuleCode(busModule).getBusCodePrefix();
	}

	public static String getIdPrefixByBusModule(String busModule) {
		return getBusSourceInfoByBusModuleCode(busModule).getIdPrefix();
	}

	/**
	 * 根据业务类型代码获取业务类型配置信息
	 * @param
	 * @return
	 */
	public static BusModuleSettingItem getBusSourceInfoByBusModuleCode(String busModule) {
		Validate.notBlank(busModule, "busModule is null");
		BusModuleSettingItem busModuleSettingItem = getBusSourceSetting().getSettingItemBySourceCode(busModule);
		if(busModuleSettingItem == null) {
			throw new BusinessException("busmodeule not set");
		}
		return busModuleSettingItem;
	}

	private static BusModuleSetting getBusSourceSetting() {
		if (busModuleSetting != null) {
			return busModuleSetting;
		}
		synchronized (BUS_CENTER_SESSING_PATH) {
			if (busModuleSetting != null) {
				return busModuleSetting;
			}
			try {
				busModuleSetting = JSON.parseObject(BusModuleSetting.class.getClassLoader().getResourceAsStream(BUS_CENTER_SESSING_PATH),
								BusModuleSetting.class);
			} catch (IOException e) {
				throw new BusinessException("load bus center setting failed", e);
			}
		}
		return busModuleSetting;
	}

}
