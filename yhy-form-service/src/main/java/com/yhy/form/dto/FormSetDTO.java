package com.yhy.form.dto;

import com.google.common.collect.Lists;
import com.yhy.common.dto.BaseMngDTO;
import com.yhy.form.vo.FormSetContentVO;
import com.yhy.form.vo.FormSetFieldPrivVO;
import com.yhy.form.vo.FormSetFieldVO;
import com.yhy.form.vo.FormSetMainVO;
import com.yhy.form.vo.mng.FormSetMngVO;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-1-10 下午2:02 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class FormSetDTO extends BaseMngDTO<FormSetMngVO> {

    private FormSetContentVO formSetContentVO;
    private List<FormSetFieldVO> formSetFieldVOList = Lists.newArrayList();
    private List<FormSetFieldPrivVO> formSetFieldPrivVOList = Lists.newArrayList();

    @Override
    public FormSetMainVO getBusMain() {
        return new FormSetMainVO();
    }

    public FormSetContentVO getFormSetContentVO() {
        if(formSetContentVO == null)
            formSetContentVO = new FormSetContentVO();
        return formSetContentVO;
    }

    public void setFormSetContentVO(FormSetContentVO formSetContentVO) {
        this.formSetContentVO = formSetContentVO;
    }

    public List<FormSetFieldVO> getFormSetFieldVOList() {
        return formSetFieldVOList;
    }

    public void setFormSetFieldVOList(List<FormSetFieldVO> formSetFieldVOList) {
        this.formSetFieldVOList = formSetFieldVOList;
    }

    public List<FormSetFieldPrivVO> getFormSetFieldPrivVOList() {
        return formSetFieldPrivVOList;
    }

    public void setFormSetFieldPrivVOList(List<FormSetFieldPrivVO> formSetFieldPrivVOList) {
        this.formSetFieldPrivVOList = formSetFieldPrivVOList;
    }

}
