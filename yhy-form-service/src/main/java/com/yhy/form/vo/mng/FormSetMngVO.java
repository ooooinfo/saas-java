package com.yhy.form.vo.mng;

import com.yhy.common.dto.BaseMngVO;

import java.util.List;
import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-1-10 上午10:53 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class FormSetMngVO extends BaseMngVO {


    private String formName;
    private Integer formWidth;
    private String oaMenuName;
    private String formType;
    private String moduleBusClass;
    private String formViewUrl;
    private String formAddUrl;
    private String formEditUrl;
    private String formDeleteUrl;

    private Set<String> formTypeList;
    private List<FormSetMngVO> children;

    public List<FormSetMngVO> getChildren() {
        return children;
    }

    public void setChildren(List<FormSetMngVO> children) {
        this.children = children;
    }

    public Set<String> getFormTypeList() {
        return formTypeList;
    }

    public void setFormTypeList(Set<String> formTypeList) {
        this.formTypeList = formTypeList;
    }

    public Integer getFormWidth() {
        return formWidth;
    }

    public void setFormWidth(Integer formWidth) {
        this.formWidth = formWidth;
    }

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    public String getModuleBusClass() {
        return moduleBusClass;
    }

    public void setModuleBusClass(String moduleBusClass) {
        this.moduleBusClass = moduleBusClass;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getOaMenuName() {
        return oaMenuName;
    }

    public void setOaMenuName(String oaMenuName) {
        this.oaMenuName = oaMenuName;
    }

    public String getFormViewUrl() {
        return formViewUrl;
    }

    public void setFormViewUrl(String formViewUrl) {
        this.formViewUrl = formViewUrl;
    }

    public String getFormAddUrl() {
        return formAddUrl;
    }

    public void setFormAddUrl(String formAddUrl) {
        this.formAddUrl = formAddUrl;
    }

    public String getFormEditUrl() {
        return formEditUrl;
    }

    public void setFormEditUrl(String formEditUrl) {
        this.formEditUrl = formEditUrl;
    }

    public String getFormDeleteUrl() {
        return formDeleteUrl;
    }

    public void setFormDeleteUrl(String formDeleteUrl) {
        this.formDeleteUrl = formDeleteUrl;
    }
}
