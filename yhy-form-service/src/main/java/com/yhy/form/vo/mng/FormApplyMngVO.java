package com.yhy.form.vo.mng;

import com.yhy.common.dto.BaseMngVO;

import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-17 上午10:57 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class FormApplyMngVO extends BaseMngVO {

    /**
     * 表单编号 db_column: FORM_ID
     */
    private String formId;
    /**
     * 申请名称 db_column: APPLY_NAME
     */
    private String applyName;
    /**
     * 系统申请编号 db_column: APPLY_CODE
     */
    private String applyCode;

    private String formType;
    private String formCode;

    /**
     * 表单名称 db_column: FORM_NAME
     */
    private String formName;
    /**
     *  表单查看地址 db_column: FORM_VIEW_URL
     */
    private String formViewUrl;
    /**
     * 表单新增地址 db_column: FORM_ADD_URL
     */
    private String formAddUrl;
    /**
     * 表单编辑后台地址 db_column: FORM_EDIT_URL
     */
    private String formEditUrl;
    /**
     * 表单删除地址 db_column: FORM_DELETE_URL
     */
    private String formDeleteUrl;

    private Set<String> formTypes;

    public Set<String> getFormTypes() {
        return formTypes;
    }

    public void setFormTypes(Set<String> formTypes) {
        this.formTypes = formTypes;
    }

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    public String getFormCode() {
        return formCode;
    }

    public void setFormCode(String formCode) {
        this.formCode = formCode;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getApplyName() {
        return applyName;
    }

    public void setApplyName(String applyName) {
        this.applyName = applyName;
    }

    public String getApplyCode() {
        return applyCode;
    }

    public void setApplyCode(String applyCode) {
        this.applyCode = applyCode;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getFormViewUrl() {
        return formViewUrl;
    }

    public void setFormViewUrl(String formViewUrl) {
        this.formViewUrl = formViewUrl;
    }

    public String getFormAddUrl() {
        return formAddUrl;
    }

    public void setFormAddUrl(String formAddUrl) {
        this.formAddUrl = formAddUrl;
    }

    public String getFormEditUrl() {
        return formEditUrl;
    }

    public void setFormEditUrl(String formEditUrl) {
        this.formEditUrl = formEditUrl;
    }

    public String getFormDeleteUrl() {
        return formDeleteUrl;
    }

    public void setFormDeleteUrl(String formDeleteUrl) {
        this.formDeleteUrl = formDeleteUrl;
    }

}
