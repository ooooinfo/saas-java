package com.yhy.form.constants;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-16 下午3:42 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class FormDesignConstant {
    public static final String ALL_FIELDS = "fields";
    public static final String FORM_WIDTH = "formWidth";

    public static final String FIELD_CONFIG_INFO = "__config__";

    public static final String FIELD_TYPE = "tag";
    public static final String FIELD_CONTROL_ID = "__vModel__";
    public static final String FIELD_NAME = "label";
    public static final String FIELD_DEFAULT_VALUE = "defaultValue";

    public static final String COL_FORM_ITEM = "colFormItem";
    public static final String LAYOUT = "layout";

    public static final String FIELD_LIST = "options";
    public static final String FIELD_LIST_VALUE = "value";
    public static final String FIELD_LIST_LABEL = "label";
    public static final String FIELD_LIST_ATTRIBUTE1 = "id";

    public static final String FIELD_CHILDREN = "children";


    public static final String REQUIRED_FLAG = "required"; // true ,false

}
