package com.yhy.form.dao;

import com.yhy.common.dao.BaseDao;
import com.yhy.form.vo.FormSetFieldVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;


/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-1-10 上午11:05 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Mapper
@Component(value = "formSetFieldDao")
public interface FormSetFieldDao extends BaseDao<FormSetFieldVO> {

    List<FormSetFieldVO> findByMainId(@Param("mainId") String mainId);

    Integer updateFieldPrivFlag(@Param("id") String id, @Param("privFlag") String privFlag);
}
