package com.yhy.form.dao;

import com.yhy.common.dao.BaseDao;
import com.yhy.form.vo.FormSetFieldListVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-15 下午2:24 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Mapper
@Component(value = "formSetFieldListDao")
public interface FormSetFieldListDao  extends BaseDao<FormSetFieldListVO> {

    List<FormSetFieldListVO> findByMainId(@Param("mainId") String mainId, @Param("fieldControlId") String fieldControlId);

    Integer deleteByMainId(@Param("mainId") String mainId, @Param("fieldControlId") String fieldControlId);

}
