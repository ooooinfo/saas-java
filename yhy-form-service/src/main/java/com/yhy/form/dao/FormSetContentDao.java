package com.yhy.form.dao;

import com.yhy.common.dao.BaseDao;
import com.yhy.form.vo.FormSetContentVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;


/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-1-10 上午11:05 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

@Mapper
@Component(value = "formSetContentDao")
public interface FormSetContentDao extends BaseDao<FormSetContentVO> {

    FormSetContentVO findByMainId(@Param("mainId") String mainId);

}
