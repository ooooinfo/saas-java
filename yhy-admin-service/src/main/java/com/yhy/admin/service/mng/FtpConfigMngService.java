package com.yhy.admin.service.mng;

import com.yhy.admin.dao.FtpConfigMngDao;
import com.yhy.admin.dto.FtpConfigDTO;
import com.yhy.admin.service.FtpConfigSetService;
import com.yhy.admin.vo.FtpConfigSet;
import com.yhy.admin.vo.mng.FtpConfigMngVO;
import com.yhy.common.config.CustomJasyptStringEncryptor;
import com.yhy.common.constants.BusModuleType;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.service.BaseMngService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-25 下午1:56 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class FtpConfigMngService extends BaseMngService<FtpConfigDTO,FtpConfigMngVO> {

    @Autowired
    private FtpConfigMngDao ftpConfigMngDao;
    @Autowired
    private FtpConfigSetService ftpConfigSetService;

    @Override
    protected FtpConfigMngDao getBaseMngDao() {
        return ftpConfigMngDao;
    }

    @Override
    public String getBusModule() {
        return BusModuleType.MD_FTP_CONFIG_MNG.getVal();
    }

    @Override
    public FtpConfigSetService getBaseMainService() {
        return ftpConfigSetService;
    }

    @Override
    protected void processOfToAdd(FtpConfigDTO baseDTO) {
        super.processOfToAdd(baseDTO);
        FtpConfigMngVO busMainData = baseDTO.getBusMainData();
        busMainData.setEnableFlag("Y");
        busMainData.setPort(21);
    }

    @Override
    protected void beforeSaveOfProcess(FtpConfigDTO baseDTO) {
        super.beforeSaveOfProcess(baseDTO);
        FtpConfigMngVO busMainData = baseDTO.getBusMainData();
        // 校验默认标识
        if("Y".equals(busMainData.getDefaultFlag())) {
            FtpConfigSet ftpConfigSet = getBaseMainService().getDefaultFtpConfig(busMainData.getSysOwnerCpy());
            if(ftpConfigSet != null && !ftpConfigSet.getId().equals(busMainData.getId())) {
                throw new BusinessException("FTP默认配置已存在."+ ftpConfigSet.getName());
            }
        }
        String password = new CustomJasyptStringEncryptor().encrypt(busMainData.getPassword());
        busMainData.setPassword(password);
    }

    @Override
    protected void afterProcessCustomDataOfSave(FtpConfigDTO baseDTO) {
        super.afterProcessCustomDataOfSave(baseDTO);
        ftpConfigSetService.removeFtpConfigByCode(baseDTO.getBusMainData().getId());
    }

    @Override
    protected void beforeDeleteOfProcess(FtpConfigMngVO mngVO) {
    }

}
