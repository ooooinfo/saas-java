package com.yhy.admin.service.mng;

import com.google.common.collect.Maps;
import com.yhy.admin.dto.SysRoleDTO;
import com.yhy.common.jxls.MyJxlsExportSupport;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-5-8 下午4:32 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Service("roleJxlsExportSupport")
public class RoleJxlsExportSupport extends MyJxlsExportSupport<SysRoleDTO> {
    @Autowired
    private SysRoleMngService roleMngService;

    @Override
    protected SysRoleMngService getBaseService() {
        return roleMngService;
    }

    @Override
    public Map<String, Object> processDatas(SysRoleDTO baseDto) {
        SysUser sysUser = YhyUtils.getSysUser();
        List<HashMap> dataList =  roleMngService.findAllPrivList(sysUser.getCpyCode(), baseDto.getUserParamBean().getUserAccount());
        Map<String, Object> rtnMap = Maps.newHashMap();
        rtnMap.put("rtnList", dataList);
        return rtnMap;
    }

}
