package com.yhy.admin.service.mng;

import com.yhy.admin.dto.CpyDTO;
import com.yhy.admin.vo.mng.CpyImportVO;
import com.yhy.admin.vo.mng.CpyMngVO;
import com.yhy.common.jxls.MyJxlsImportSupport;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-23 下午9:33 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Service("cpyJxlsImportSupport")
public class CpyJxlsImportSupport extends MyJxlsImportSupport<CpyDTO> {

	@Override
	public void handleData(Map<String, Object> datas) {
		super.handleData(datas);
		List<CpyImportVO> importBeans = (List<CpyImportVO>) datas.get(MyJxlsImportSupport.IMPORT_DATA_KEY);
		//根据导入的Bean处理信息
		System.out.println(importBeans.size()+"daf");
		//执行导入
	}

}
