package com.yhy.admin.dao;

import com.yhy.admin.vo.SysRoleVO;
import com.yhy.common.dao.BaseDao;
import com.yhy.common.dao.BaseMainDao;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-8 下午6:20 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Mapper
@Component(value = "sysRoleDao")
public interface SysRoleDao extends BaseMainDao<SysRoleVO> {


}
