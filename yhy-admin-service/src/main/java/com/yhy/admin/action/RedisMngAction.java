package com.yhy.admin.action;

import com.google.common.collect.Sets;
import com.yhy.admin.dto.RedisDTO;
import com.yhy.admin.service.mng.RedisMngService;
import com.yhy.common.action.BaseMngAction;
import com.yhy.common.dto.AppReturnMsg;
import com.yhy.common.dto.ReturnCode;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.intercept.PreCheckPermission;
import com.yhy.common.utils.RedisUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-14 下午5:23 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *  
 */
@RestController
@RequestMapping(value="/admin-service/api/redisMng", produces="application/json;charset=UTF-8")
public class RedisMngAction extends BaseMngAction<RedisDTO> {

    @Autowired
    private RedisMngService redisMngService;

    @Override
    protected RedisMngService getBaseService() {
        return redisMngService;
    }


    @PreCheckPermission(roles = {"SYS_ADMIN","REDIS_ALL","REDIS_CREATE","REDIS_CHANGE"})
    @RequestMapping(value="/doSave",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存", notes="保存操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "BaseDTO")
    })
    public AppReturnMsg doSave(HttpServletRequest request, HttpServletResponse response, @RequestBody RedisDTO baseDTO) {
        return super.doSaveCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","REDIS_ALL","REDIS_SELECT"})
    @RequestMapping(value="/loadData",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="查询数据", notes="查询操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "BaseDTO")
    })
    public AppReturnMsg loadData(HttpServletRequest request, HttpServletResponse response, @RequestBody RedisDTO baseDTO) {
        return super.loadDataCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","REDIS_ALL","REDIS_DELETE"})
    @RequestMapping(value="/doDelete",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="删除", notes="删除操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "删除数组", required = true, dataType = "BaseDTO")
    })
    public AppReturnMsg doDelete(HttpServletRequest request, HttpServletResponse response, @RequestBody RedisDTO baseDTO) {
        return this.doDeleteCommon(request, response, baseDTO);
    }

    @Override
    protected AppReturnMsg doDeleteCommon(HttpServletRequest request, HttpServletResponse response, RedisDTO baseDTO) {
        if (org.springframework.util.CollectionUtils.isEmpty(baseDTO.getIdList())) {
            throw new BusinessException("删除key不能为空.");
        }

        List<String> baseEntities = baseDTO.getIdList();

        for (String tmpId : baseEntities) {
            if("*".equals(tmpId)) {
                Set<String> keys = RedisUtil.getRedisTemplate().keys("*" + tmpId);
                if (!CollectionUtils.isEmpty(keys)) {
                    RedisUtil.getRedisTemplate().delete(keys);
                }
            } else {
                Set<String> keys = Sets.newHashSet(tmpId);
                RedisUtil.getRedisTemplate().delete(keys);
            }
        }
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"删除成功","");
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","REDIS_ALL","REDIS_DELETE"})
    @RequestMapping(value="/doDeleteAll",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="删除", notes="删除操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "删除数组", required = true, dataType = "BaseDTO")
    })
    public AppReturnMsg doDeleteAll(HttpServletRequest request, HttpServletResponse response, @RequestBody RedisDTO baseDTO) {
        return this.doDeleteCommon(request, response, baseDTO);
    }

}
