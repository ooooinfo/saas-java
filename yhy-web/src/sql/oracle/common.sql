/*==============================================================*/
/* Table: t_bus_common_state                                    */
/*==============================================================*/
create table t_bus_common_state
(
   id                   varchar2(40)                   not null,
   main_id              varchar2(40)                   not null,
   self_state           number(2)                      not null,
   other_state          number(2)                      null,
   bus_type             number(2)                      null,
   bus_date             date                           null,
   bus_direction        number(2)                      null,
   bus_module           varchar2(10)                   null,
   bus_class            varchar2(20)                   null,
   fun_process          number(2)                      null,
   rel_num              varchar2(40)                   null,
   encrypt_code         varchar2(100)                  null,
   forward_type         number(2)                      null,
   parent_id            varchar2(40)                   null,
   custom_code          varchar2(100)                  null,
   sys_gen_code         varchar2(100)                  null,
   create_date          date                           not null,
   last_update_date     date                           null,
   create_by            varchar2(40)                   null,
   last_update_by       varchar2(40)                   null,
   sys_org_code         varchar2(40)                   null,
   sys_owner_cpy        varchar2(40)                   null,
   other_sys_org_code   varchar2(40)                   null,
   other_sys_owner_cpy  varchar2(40)                   null,
   rmk                  varchar2(1000)                 null,
   enable_flag          varchar2(1)                    null,
   attribute1           varchar2(100)                  null,
   attribute2           varchar2(100)                  null,
   version              number(8)                      null,
   history_flag         varchar2(1)                    null,
   constraint PK_T_BUS_COMMON_STATE primary key (id)
);

comment on table t_bus_common_state is
'业务状态表(模块公用表)';

comment on column t_bus_common_state.id is
'主键(sys_code+UUID)';

comment on column t_bus_common_state.main_id is
'主表ID';

comment on column t_bus_common_state.self_state is
'自身单据状态';

comment on column t_bus_common_state.history_flag is
'历史标识';

comment on column t_bus_common_state.other_state is
'对方接受的单据状态';

comment on column t_bus_common_state.bus_type is
'业务类别(即时生效或待对方同意生效)';

comment on column t_bus_common_state.bus_date is
' 业务时间';

comment on column t_bus_common_state.bus_direction is
'业务方向(1:主动发起, 2: 被接受)';

comment on column t_bus_common_state.bus_module is
'业务模块';

comment on column t_bus_common_state.fun_process is
'功能处理(新增，变更，注销)';

comment on column t_bus_common_state.rel_num is
'关联号(同一张单据的匹配号)';

comment on column t_bus_common_state.encrypt_code is
'加密数据串(防止数据被更改)';

comment on column t_bus_common_state.forward_type is
'转发类别(0:不转发,1:统一转发，2:转发给对应的当事人)';

comment on column t_bus_common_state.last_update_date is
'最后更新时间';

comment on column t_bus_common_state.last_update_by is
'最后更新人';
comment on column t_bus_common_state.parent_id is
'原ID（变更，注销对应的原ID）';

comment on column t_bus_common_state.custom_code is
'自定义编号';

comment on column t_bus_common_state.sys_gen_code is
'系统产生编号';

comment on column t_bus_common_state.sys_org_code is
'用户所属组织部门';

comment on column t_bus_common_state.sys_owner_cpy is
'用户所属公司';

comment on column t_bus_common_state.other_sys_org_code is
'对方组织';

comment on column t_bus_common_state.other_sys_owner_cpy is
'对方公司';

comment on column t_bus_common_state.rmk is
'备注';

comment on column t_bus_common_state.enable_flag is
'有效标识';

comment on column t_bus_common_state.attribute1 is
'扩展属性1 ';

comment on column t_bus_common_state.attribute2 is
'扩展属性二';

comment on column t_bus_common_state.version is
'更新版本号';

-- Create/Recreate indexes
create index BUS_COMMON_STATE_IDX_MAINID on T_BUS_COMMON_STATE (main_id);
create index BUS_COMMON_STATE_IDX_cpy on T_BUS_COMMON_STATE (sys_owner_cpy,history_flag,self_state);

-- Add/modify columns
alter table T_BUS_COMMON_STATE add bus_version NUMBER(3) default 1;
-- Add comments to the columns
comment on column T_BUS_COMMON_STATE.bus_version
is '业务版本号';

/*==============================================================*/
/* Table: th_bus_common_state                                    */
/*==============================================================*/
create table th_bus_common_state
(
   id                   varchar2(40)                   not null,
   main_id              varchar2(40)                   not null,
   self_state           number(2)                      not null,
   other_state          number(2)                      null,
   bus_type             number(2)                      null,
   bus_date             date                           null,
   bus_direction        number(2)                      null,
   bus_module           varchar2(10)                   null,
   bus_class           varchar2(20)                   null,
   fun_process          number(2)                      null,
   rel_num              varchar2(40)                   null,
   encrypt_code         varchar2(100)                  null,
   forward_type         number(2)                      null,
   parent_id            varchar2(40)                   null,
   custom_code          varchar2(100)                  null,
   sys_gen_code         varchar2(100)                  null,
   create_date          date                           not null,
   last_update_date     date                           null,
   create_by            varchar2(40)                   null,
   last_update_by       varchar2(40)                   null,
   sys_org_code         varchar2(40)                   null,
   sys_owner_cpy        varchar2(40)                   null,
   other_sys_org_code   varchar2(40)                   null,
   other_sys_owner_cpy  varchar2(40)                   null,
   rmk                  varchar2(1000)                 null,
   enable_flag          varchar2(1)                    null,
   attribute1           varchar2(100)                  null,
   attribute2           varchar2(100)                  null,
   version              number(8)                      null,
   history_flag         varchar2(1)                    null,
   constraint PK_th_bus_common_state primary key (id)
);

comment on table th_bus_common_state is
'业务状态历史表(模块公用表)';

comment on column th_bus_common_state.id is
'主键(sys_code+UUID)';

comment on column th_bus_common_state.main_id is
'主表ID';

comment on column th_bus_common_state.self_state is
'自身单据状态';

comment on column th_bus_common_state.history_flag is
'历史标识';

comment on column th_bus_common_state.other_state is
'对方接受的单据状态';

comment on column th_bus_common_state.bus_type is
'业务类别(即时生效或待对方同意生效)';

comment on column th_bus_common_state.bus_date is
' 业务时间';

comment on column th_bus_common_state.bus_direction is
'业务方向(1:主动发起, 2: 被接受)';

comment on column th_bus_common_state.bus_module is
'业务模块';

comment on column th_bus_common_state.fun_process is
'功能处理(新增，变更，注销)';

comment on column th_bus_common_state.rel_num is
'关联号(同一张单据的匹配号)';

comment on column th_bus_common_state.encrypt_code is
'加密数据串(防止数据被更改)';

comment on column th_bus_common_state.forward_type is
'转发类别(0:不转发,1:统一转发，2:转发给对应的当事人)';

comment on column th_bus_common_state.last_update_date is
'最后更新时间';

comment on column th_bus_common_state.last_update_by is
'最后更新人';
comment on column th_bus_common_state.parent_id is
'原ID（变更，注销对应的原ID）';

comment on column th_bus_common_state.custom_code is
'自定义编号';

comment on column th_bus_common_state.sys_gen_code is
'系统产生编号';

comment on column th_bus_common_state.sys_org_code is
'用户所属组织部门';

comment on column th_bus_common_state.sys_owner_cpy is
'用户所属公司';

comment on column th_bus_common_state.other_sys_org_code is
'对方组织';

comment on column th_bus_common_state.other_sys_owner_cpy is
'对方公司';

comment on column th_bus_common_state.rmk is
'备注';

comment on column th_bus_common_state.enable_flag is
'有效标识';

comment on column th_bus_common_state.attribute1 is
'扩展属性1 ';

comment on column th_bus_common_state.attribute2 is
'扩展属性二';

comment on column th_bus_common_state.version is
'更新版本号';

-- Create/Recreate indexes
create index BUS_COMMON_STATE_IDXH_MAINID on th_bus_common_state (main_id);
create index BUS_COMMON_STATE_IDXH_cpy on TH_BUS_COMMON_STATE (sys_owner_cpy,history_flag,self_state);

-- Add/modify columns
alter table th_bus_common_state add bus_version NUMBER(3) default 1;
-- Add comments to the columns
comment on column th_bus_common_state.bus_version
is '业务版本号';

/*==============================================================*/
/* Table: t_sys_attachment                                      */
/*==============================================================*/
create table t_sys_attachment
(
   id                   varchar2(40)                   not null,
   main_id              varchar2(40)                   null,
   bus_module           varchar2(10)                   null,
   file_name            varchar2(100)                  null,
   file_type            varchar2(6)                    null,
   file_size            number(8)                      null,
   file_path            varchar2(100)                  null,
   file_url             varchar2(200)                  null,
   create_date          date                           not null,
   last_update_date     date                           null,
   create_by            varchar2(40)                   null,
   last_update_by       varchar2(40)                   null,
   sys_org_code         varchar2(40)                   null,
   sys_owner_cpy        varchar2(40)                   null,
   rmk                  varchar2(1000)                 null,
   enable_flag          varchar2(1)                    null,
   attribute1           varchar2(100)                  null,
   attribute2           varchar2(100)                  null,
   version              number(8)                      null,
   constraint PK_T_SYS_ATTACHMENT primary key (id)
);

comment on table t_sys_attachment is
'附件表';

comment on column t_sys_attachment.id is
'主键(sys_code+UUID)';

comment on column t_sys_attachment.main_id is
'主表ID';

comment on column t_sys_attachment.bus_module is
'业务模块';

comment on column t_sys_attachment.file_name is
'文件名';

comment on column t_sys_attachment.file_type is
'文件类型';

comment on column t_sys_attachment.file_size is
'文件大小(Byte)';

comment on column t_sys_attachment.file_path is
'文件路径';

comment on column t_sys_attachment.file_url is
'文件访问URL';

comment on column t_sys_attachment.last_update_date is
'最后更新时间';

comment on column t_sys_attachment.last_update_by is
'最后更新人';

comment on column t_sys_attachment.sys_org_code is
'用户所属组织部门';

comment on column t_sys_attachment.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_attachment.rmk is
'备注';

comment on column t_sys_attachment.enable_flag is
'有效标识';

comment on column t_sys_attachment.attribute1 is
'扩展属性1 ';

comment on column t_sys_attachment.attribute2 is
'扩展属性二';

comment on column t_sys_attachment.version is
'更新版本号';

/*==============================================================*/
/* Table: t_bus_extend                                        */
/*==============================================================*/
create table t_bus_extend
(
   id                 VARCHAR2(40)         not null,
   main_id            VARCHAR2(40),
   form_apply_id      VARCHAR2(40),
   source_form_apply_id VARCHAR2(40),
   form_id            VARCHAR2(40),
   form_code          VARCHAR2(40),
   bus_Module         VARCHAR2(10),
   bus_class          VARCHAR2(10),
   create_date        DATE                 not null,
   last_update_date   DATE,
   create_by          VARCHAR2(40),
   last_update_by     VARCHAR2(40),
   sys_org_code       VARCHAR2(40),
   sys_owner_cpy      VARCHAR2(40),
   rmk                VARCHAR2(1000),
   enable_flag        CHAR(1),
   version            NUMBER(8,0),
   attribute1         VARCHAR2(400),
   attribute2         VARCHAR2(400),
   attribute3         VARCHAR2(400),
   attribute4         VARCHAR2(400),
   attribute5         VARCHAR2(400),
   attribute6         VARCHAR2(400),
   attribute7         NUMBER(26,6),
   attribute8         NUMBER(26,6),
   attribute9         DATE,
   attribute10        DATE,
   constraint PK_T_BUS_EXTEND primary key (id)
);

comment on table t_bus_extend is
'业务扩展表(与表单进行关联)';

comment on column t_bus_extend.id is
'主键';

comment on column t_bus_extend.main_id is
'主表ID';

comment on column t_bus_extend.form_apply_id is
'表单申请Id';

comment on column t_bus_extend.source_form_apply_id is
'表单申请ID(复制或变更带过来的ID)';

comment on column t_bus_extend.form_id is
'表单设置ID';

comment on column t_bus_extend.form_code is
'表单编码';

comment on column t_bus_extend.bus_Module is
'模块';

comment on column t_bus_extend.bus_class is
'业务种类';

comment on column t_bus_extend.last_update_date is
'最后更新时间';

comment on column t_bus_extend.last_update_by is
'最后更新人';

comment on column t_bus_extend.sys_org_code is
'用户所属组织部门';

comment on column t_bus_extend.sys_owner_cpy is
'用户所属公司';

comment on column t_bus_extend.rmk is
'备注';

comment on column t_bus_extend.enable_flag is
'有效标识';

comment on column t_bus_extend.attribute1 is
'扩展属性1 ';

comment on column t_bus_extend.attribute2 is
'扩展属性二';

comment on column t_bus_extend.attribute7 is
'更新版本号';

/*==============================================================*/
/* Index: bus_extend_idx_mainid                               */
/*==============================================================*/
create index bus_extend_idx_mainid on t_bus_extend (
   main_id ASC,
   enable_flag ASC
);
